from flask import Flask, jsonify
import os

app = Flask(__name__)

devices = ['0x06c0', '0x0c51', '0x4e18', '0x14fb']
data_path = "/home/jnct/dev/mesh/data"

light_template = 'light_avg_{}.txt'
temp_template = 'temp_avg_{}.txt'

verdict_light_filename = 'verdict_light.txt'
verdict_temp_filename = 'verdict_temp.txt'


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/light')
def light():
    res = dict()
    for device in devices:
        device_filename = light_template.format(device)
        device_path = os.path.join(data_path, device_filename)
        with open(device_path, 'r') as f:
            res[device] = f.read()
    states = jsonify(res)
    states.headers['Access-Control-Allow-Origin'] = '*'
    return states


@app.route('/temp')
def temp():
    res = dict()
    for device in devices:
        device_filename = temp_template.format(device)
        device_path = os.path.join(data_path, device_filename)
        with open(device_path, 'r') as f:
            res[device] = f.read()
    states = jsonify(res)
    states.headers['Access-Control-Allow-Origin'] = '*'
    return states


@app.route('/verdicts')
def verdicts():
    with open(os.path.join(data_path, verdict_light_filename), 'r') as f:
        vlight = f.read()
    with open(os.path.join(data_path, verdict_temp_filename), 'r') as f:
        vtemp = f.read()
    verdicts = jsonify(
        verdict_light=vlight,
        verdict_temp=vtemp
    )
    verdicts.headers['Access-Control-Allow-Origin'] = '*'
    return verdicts


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000, debug=True)
